// -------------------------------------
//               ELEMENTS
// -------------------------------------
const root = document.getElementById('root');

// -------------------------------------
//              INIT SCREEN
// -------------------------------------
const screen = document.createElement('div');
screen.id = "screen";
root.appendChild(screen);

// -------------------------------------
//               GAMEOBJECT
// -------------------------------------
class GameObject {
    constructor(className, bottom, right, width, height, border, background) {
        this.className         = className;
        this.bottom       = bottom;
        this.right        = right;
        this.width        = `${width}px`;
        this.height       = `${height}px`;
        this.border       = border;
        this.background   = background;
    }

    DOMappend() {
        // CREATE ELEMENT
        const elem = document.createElement('div');
        const bd   = this.border;

        // STYLIZE ELEMENT
        elem.classList                 = this.className;
        elem.style.bottom              = `${this.bottom}px`;
        if (this.right !== undefined)
            elem.style.right           = `${this.right}px`;
        elem.style.width               = this.width;
        elem.style.height              = this.height;
        elem.style.border              = `${bd.size}px ${bd.type} ${bd.color}`;
        elem.style.borderRadius        = `${bd.radius}px`
        elem.style.backgroundColor     = this.background;
        // APPEND

        screen.appendChild(elem);
    }
}

// -------------------------------------
//               PERSONNAGE
// -------------------------------------
class Perso extends GameObject {
    constructor(isJumping) {
        super(...arguments),
        this.isJumping = isJumping
    }

    jump() {
        const perso = document.querySelector(`.perso`);

        if (!this.isJumping) {
            this.isJumping = true;
            perso.classList.add('bounce');
            setTimeout(() => {
                perso.classList.remove('bounce');
                this.isJumping = false;
            }, 500)
        }
    }
    backWard() {
        const perso = document.querySelector(`.perso`);
        perso.style.right = this.right;
    }
    forWard() {
        const perso = document.querySelector(`.perso`);
        this.right -= 3;
        perso.style.right = this.right;
    }
}

/* Floor */
const floor = new GameObject('floor', 0, undefined, 700, 10, {size: 1, type: 'solid', color: 'green', radius: 0}, 'green');
floor.DOMappend();

/* Obstacle */
const obstacle = new GameObject('obstacle', 12, 25, 80, 120, {size: 2, type: 'solid', color: '#eae060', radius: 5}, '#eae060');
obstacle.DOMappend();

/* Personnage */
const perso = new Perso('perso', 12, 600, 30, 30, {size: 2, type: 'solid', color: '#000', radius: 20}, "red", false);
perso.DOMappend();

/* Commands */
const controller = {
    32: {pressed: false, func: perso.jump},
    37: {pressed: false, func: perso.backWard},
    39: {pressed: false, func: perso.forWard}
}

// -------------------------------------
//                EVENTS
// -------------------------------------
document.addEventListener('keydown', (e) => {
    if (controller[e.keyCode]) {
        controller[e.keyCode].pressed = true;
        controller[e.keyCode].func();
    }
})

document.addEventListener('keyup', (e) => {
    if (controller[e.keyCode]) {
        controller[e.keyCode].pressed = false;
    }
})